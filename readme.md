# eveAPI  

_REST API for fetching Suricatas eve.json_  
_by jmbluethner_

<img src="https://img.shields.io/badge/License-WTFPL-green">
<img src="https://img.shields.io/badge/Built_With-Node.js-blue">

---

## ❓ What is eveAPI?  

eveAPI is a REST API, made with NodeJS and Express, that allows you to fetch parts of the eve.json from [Suricata](https://suricata.io/).  
It was primarily made to allow Grafana to access Suricata logs with the [simpod-json-datasource](https://grafana.com/grafana/plugins/simpod-json-datasource/) plugin.  
Currently, the only proper way to extract the eve.json, and to make it readable for Grafana, is to use Elasticsearch. However, in order to achieve that, you need to set up Filebeat, Kibana and the Filebeat Suricata Plugin in addition to Elastic. So, as you can imagine, the setup can be described as absolute pain.  
That's why I wanted to come up with a way simpler solution. And I think I've achieved that.

## 📚 Features

- Open REST API
- Full Documentation
- Basic Auth capabilities
- Origin IP whitelisting
- Error & Access logging
- Colorful live terminal logging

## 🚀 Setup  

### Prerequisites  

The only thing that's required is a working installation of [Node.js](https://nodejs.org)  

### Installation  

Grab the current Codebase from the master branch on your Suricata machine.

```shell
git clone -b master https://gitlab.com/jmbluethner/eve-api
```  

Move all files from `src` to the main directory

```shell
mv ./eve-api/src/* ./
```

Create the `.env` file to configure **eveAPI** the way you want it.  
Creating the `.env` **is mandatory** because the `EVEJSON` parameter is essential. All other settings have default values. However, it is strongly recommended to create your own full `.env`, especially when it comes to **eveAPI**'s security features.

```shell
nano .env
```

```dotenv
# Defaults to 3300
PORT="<Port to use>"

# Log locations for eveAPI access and error logs.
# Defaults to /var/log/eveAPI/error.log
ERROR_LOG_LOCATION="<log file location>"
# Defaults to /var/log/eveAPI/access.log
ACCESS_LOG_LOCATION="<log file location>"

# Ususally located at /var/log/suricata/eve.json
# It is recommended to use an absolute path tp the file
EVEJSON="<location of your eve.json>"

# Credentials for basic auth. No auth will be required if _USER or _PASS are empty.
BASICAUTH_USER="<user>"
BASICAUTH_PASS="<pass>"

# Whitelist all clients that are allowed to access eveAPI. Leave empty to allow all hosts.
IP_WHITELIST="192.168.0.10,192.168.0.11,192.168.0.12"
```

Install Dependencies  

```shell
npm i
```

Start **eveAPI**

```shell
node app.js
```

<img src="./doc/log_out.jpg">  

After you have successfully started **eveAPI**, you can have a look at http://127.0.0.1:3300 (ofc you'll have to replace the address with the one your host is running) to verify that everything is good to go.  

<img src="./doc/frontend.jpg">

> ⚠ When Basic Auth is enabled, you'll receive a HTTP 401 response when not sending the auth headers. Browsers will not prompt you to enter the credentials. If you want to check the operational status via the frontend page while Basic Auth is enabled, use something like Postman and add the Authorization header. 

## 🔀 Endpoints

> TBD

## ⏩ On the long run  

### Security  

Make sure to protect access to the API as good as you can. Use the above explained Basic Auth and IP whitelisting capabilities of **eveAPI** and make sure to further restrict access to the API by installing something like `ufw` on your host.  
Publishing your `eve.json` to unknown clients might be quite dangerous since critical details about your network infrastructure and traffic patterns might get exposed.

### Usability  

It is recommended to create a `sysctl` service for **eveAPI** in order to automatically start it upon boot.

---

_Developed with ♥ and a shit ton of ☕ by jmbluethner_