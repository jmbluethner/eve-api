const fs = require('fs');
const moment = require('moment');
require('dotenv').config();
const chalk = require('chalk');

let locations = {
	access: '/var/log/eveAPI/access.log',
	error: '/var/log/eveAPI/error.log'
}

/**
 * @description Global function for writing to log files
 * @param {String} type Can be either 'access' or 'error'
 * @param {String} message The message to log
 * @param {Boolean} consoleOutput Controls if the message should also be printed to the console
 * @return {boolean} Returns true if the logging worked, returns false on error
 */
function logger(type,message,consoleOutput = true) {

	if(type !== 'access' && type !== 'error') return false;

	if(process.env.ACCESS_LOG_LOCATION) locations.access = process.env.ACCESS_LOG_LOCATION
	if(process.env.ERROR_LOG_LOCATION) locations.error = process.env.ERROR_LOG_LOCATION;

	let datetime = moment().format('yyyy-MM-DD|hh:mm:ss');

	let msg = '[eveAPI][' + datetime + '] ' + message;

	try {
		if(type === 'access') fs.appendFileSync(locations.access,msg + '\r\n');
		if(type === 'error') fs.appendFileSync(locations.error,msg + '\r\n');
	} catch(err) {
		console.error(chalk.bgRedBright('Error while trying to write to Log! Details: ' + err));
		console.log("");
		process.exit();
		return false;
	}

	if(consoleOutput && type === 'access') console.log(chalk.blue('[' + type + ']') + chalk.gray('[eveAPI][' + datetime + '] ') + message);
	if(consoleOutput && type === 'error') console.log(chalk.red('[' + type + ']') + chalk.gray('[eveAPI][' + datetime + '] ') + message);

	return true;

}

module.exports.logger = logger;