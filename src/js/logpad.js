function repeatStr(ch, n) {
	let res = "";
	while (n-- > 0) res += ch;
	return res;
}

function pad(str,len) {

	let strLen = str.length;
	let res = "";

	res = repeatStr(" ",len-strLen);

	return res;
}

module.exports.pad = pad;