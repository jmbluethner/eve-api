require('dotenv').config();
const chalk = require('chalk');
const {pad} = require("./logpad");

/**
 * @description Log message when starting the application
 * @param authState
 * @param port
 */
function startlog(authState,port) {

	let env = {
		port: port,
		authState: authState,
		log_access:'/var/log/eveAPI/access.log',
		log_error: '/var/log/eveAPI/error.log',
		eveJSON: process.env.EVEJSON
	}

	if(process.env.ACCESS_LOG_LOCATION) env.log_access = process.env.ACCESS_LOG_LOCATION
	if(process.env.ERROR_LOG_LOCATION) env.log_error = process.env.ERROR_LOG_LOCATION;

	if(process.env.IP_WHITELIST && process.env.IP_WHITELIST.length > 0) {
		env.whitelist = 'enabled';
	} else {
		env.whitelist = 'disabled';
	}

	const padLen = 18;

	console.log("");
	console.log(chalk.bold("eveAPI"));
	console.log(chalk.gray.italic("The eve.json REST API"));
	console.log("");
	console.log(chalk.blueBright("🌍 Port:") + pad("🌍 Port:",padLen) + env.port);
	console.log(chalk.blueBright("📁 eve.json:") + pad("📁 eve.json:",padLen) + env.eveJSON);

	if(env.authState === 'disabled') {
		console.log(chalk.blueBright("🔒 Basic Auth:") + pad("🔒 Basic Auth:",padLen) + chalk.bgRed(env.authState));
	} else {
		console.log(chalk.blueBright("🔒 Basic Auth:") + pad("🔒 Basic Auth:",padLen) + chalk.bgGreen(env.authState));
	}

	if(env.whitelist === 'disabled') {
		console.log(chalk.blueBright("🔒 IP Whitelist:") + pad("🔒 IP Whitelist:",padLen) + chalk.bgRed(env.whitelist));
	} else {
		console.log(chalk.blueBright("🔒 IP Whitelist:") + pad("🔒 IP Whitelist:",padLen) + chalk.bgGreen(env.whitelist));
	}

	console.log(chalk.blueBright("📝 Error Logs:") + pad("📝 Error Logs:",padLen) + env.log_error);
	console.log(chalk.blueBright("📝 Access Logs:") + pad("📝 Access Logs:",padLen) + env.log_access);
	console.log("");
	console.log("Logging Starts below");
	console.log("");

}

module.exports.startlog = startlog;