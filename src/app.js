const basicAuth = require('express-basic-auth');
const express = require('express');
const {startlog} = require("./js/startlog");
const app = express();
const chalk = require('chalk')
const fs = require('fs');
const {logger} = require("./js/logger");

require('dotenv').config();

if(!process.env.EVEJSON) {
	console.log("");
	console.error(chalk.bgRedBright("EVEJSON environment variable missing! Exiting ..."));
	console.log("");
	process.exit();
}

if(!fs.existsSync(process.env.EVEJSON)) {
	console.log("");
	console.error(chalk.bgRedBright("eve.json should be at " + process.env.EVEJSON + ' according to your configuration, but the given file doesn\'t exist. Exiting ... '));
	console.log("");
	process.exit();
}

const routes = {
	eve: require("./routes/eve")
}

function setHeaders(res) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET');
	res.setHeader('Access-Control-Allow-Headers', '*');
}

function logRequest(req,res) {
	logger('access','[' + req.connection.server._connectionKey + '] ➡ ' + req.method + " " + req.url + " | Origin: " + req.ip + " (" + (req.headers['x-forwarded-for'] || req.socket.remoteAddress) + ")");
	if(res) {
		logger('access','[' + req.connection.server._connectionKey + '] ⬅ ' + "RES" + " " + res.statusCode + " | For: " + req.url + " | Destination: " + req.ip + " (" + (req.headers['x-forwarded-for'] || req.socket.remoteAddress) + ")");
	}
}

const authUser = process.env.BASICAUTH_USER || null;
const authPass = process.env.BASICAUTH_PASS || null;

const middlewaresAuth = [
	function (req, res, next) {setHeaders(res);logRequest(req,res);next();},
	basicAuth({users: {[authUser]: authPass}})
];
const middlewaresOpen = [
	function (req, res, next) {setHeaders(res);logRequest(req,res);next();}
];


if(process.env.BASICAUTH_USER && process.env.BASICAUTH_PASS) {
	app.use(middlewaresAuth);
} else {
	app.use(middlewaresOpen);
}

app.use(express.json({limit: '1mb'}));

app.use('/api/eve',routes.eve);

const port = Number(process.env.PORT) || 3300;

let authState = '';
if(authUser && authPass) {
	authState = 'enabled';
} else {
	authState = 'disabled';
}

app.use('/',express.static(__dirname + '/frontend', {
	extensions: ['html', 'htm']
}));

app.listen(port, () => {
	startlog(authState,port);
});